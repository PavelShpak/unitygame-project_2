﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereController : MonoBehaviour
{
    [SerializeField] private InputHandler _inputHandler = null;
    [SerializeField] private GameObject _cubePrefab = null;
    [SerializeField] private float _Speed = 0f;

    private void OnEnable()
    {
        _inputHandler.OnKeyDown += OnKeyDown;
        //_inputHadler.SetAction(OnKeyDown);
    }

    private void OnDisable()
    {
        _inputHandler.OnKeyDown -= OnKeyDown;
    }

    private void OnKeyDown(KeyCode keyCode)
    {
        switch (keyCode)
        {
            case KeyCode.UpArrow:
                Move(Vector3.up);
                break;
            case KeyCode.DownArrow:
                Move(Vector3.down);
                break;
            case KeyCode.RightArrow:
                Move(Vector3.right);
                break;
            case KeyCode.LeftArrow:
                Move(Vector3.left);
                break;
            case KeyCode.Space:
                GenerateCube();
                break;
        }
    }

    private void Move(Vector3 direction)
    {
        transform.Translate(_Speed * Time.deltaTime * direction);
    }

    private void GenerateCube()
    {
        GameObject cube = Instantiate(_cubePrefab);
        cube.transform.position = transform.position;
    }
}

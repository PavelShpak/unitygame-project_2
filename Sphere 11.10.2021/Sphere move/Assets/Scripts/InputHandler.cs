﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public System.Action<KeyCode> OnKeyDown = null;

    public void SetAction(System.Action<KeyCode> action)
    {
        OnKeyDown = action;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            OnKeyDown?.Invoke(KeyCode.UpArrow);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            OnKeyDown?.Invoke(KeyCode.DownArrow);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            OnKeyDown?.Invoke(KeyCode.RightArrow);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            OnKeyDown?.Invoke(KeyCode.LeftArrow);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnKeyDown?.Invoke(KeyCode.Space);
        }
    }
}

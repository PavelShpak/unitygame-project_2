﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private List<Level> _levels;
    [SerializeField] private UIGameScreen _uIgameScreen = null;
    private GameObject _levelObject = null;
    [SerializeField] private GameObject _startScreen = null;
    [SerializeField] private GameObject _winScreen = null;
    [SerializeField] private GameObject _gameScreen = null;

    public void Start()
    {
        GameAnalyticsSDK.GameAnalytics.Initialize();
        _startScreen.SetActive(true);
    }

    public void StartGame()
    {
        int index = Random.Range(0, _levels.Count);
        _gameScreen.SetActive(true);
        _startScreen.SetActive(false);
        InstantiateLevel(index);
    }
    public void HomeGame()
    {
        _startScreen.SetActive(true);
        _winScreen.SetActive(false);
    }

    private void InstantiateLevel(int index)
    {
        if (_levelObject != null)
        {
            Destroy(_levelObject);
            _levelObject = null;
        }
        if (index >= _levels.Count)
        {
            return;
        }
        _levelObject = Instantiate(_levels[index].gameObject, transform);
        Level level = _levelObject.GetComponent<Level>();
        level.Initialize(_gameScreen);
        _uIgameScreen.Initialize(level);
        level.OnLevelComplete = OnLevelComplete;
    }
    private void OnLevelComplete()
    {
        _winScreen.SetActive(true);
        _gameScreen.SetActive(false);
    }
}
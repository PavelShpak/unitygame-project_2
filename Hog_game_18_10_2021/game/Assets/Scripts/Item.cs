﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Item : MonoBehaviour
{
    [SerializeField] private string _name = string.Empty;

    [Header("Settings")]
    [SerializeField] private float _scaleMultiplier;
    [SerializeField] private float _durationScale;
    [SerializeField] private float _disapearTime;

    private SpriteRenderer _spriteRenderer;

    public System.Action<Item> OnFind;
    //public delegate void Delegate();
    //public Delegate _delegate;

    public string Name { get { return _name; } set { _name = value; } }
    public Sprite ItemSprite => _spriteRenderer.sprite;
    public void Initislized()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
    private void OnMouseDown()
    {
        FindAction();
    }
    private void FindAction()
    {
        Vector3 newScale = transform.localScale * _scaleMultiplier;
        transform.DOScale(newScale, _durationScale).OnComplete(()=> 
        {
            _spriteRenderer.DOFade(0f, _disapearTime).OnComplete(DisableObject);
        });
    }
    private void DisableObject()
    {
        gameObject.SetActive(false);
        OnFind?.Invoke(this);
    }
}
